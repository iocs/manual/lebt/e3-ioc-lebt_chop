require essioc

require admisc, 2.1.2+0
require busy, 1.7.3+2
require adps4000a, 1.0.0+1
require xtpico, 0.9.3+1
require fug, 3.1.3+1

#- avoid messages 'callbackRequest: cbLow ring buffer full'
callbackSetQueueSize(10000)

epicsEnvSet(LEBT_P, "LEBT-Chop:")

iocshLoad("$(essioc_DIR)/common_config.iocsh")

#- XT Pico @ XT-PICO-XXL [CHOPPER]
epicsEnvSet(CHOP_PICO_R, "Ctrl-IM-01:")
epicsEnvSet(CHOP_PICO_HOST, "lebt-chop-lvmod.tn.esss.lu.se")
epicsEnvSet(CHOP_PICO_PORT, "1002")
epicsEnvSet(CHOP_PICO_PORT_NAME, "AK_I2C_COMM")
epicsEnvSet(I2C_TCA9555_PORT, "AK_I2C_TCA9555")
epicsEnvSet(I2C_ADT7420_PORT, "AK_I2C_ADT7420")
epicsEnvSet(I2C_LTC2991_PORT, "AK_I2C_LTC2991")

drvAsynIPPortConfigure($(CHOP_PICO_PORT_NAME), "$(CHOP_PICO_HOST):$(CHOP_PICO_PORT)")

AKI2CTCA9555Configure($(I2C_TCA9555_PORT), $(CHOP_PICO_PORT_NAME), 1, "0x21", 1, 0, 0)
dbLoadRecords("AKI2C_TCA9555.db", "P=$(LEBT_P), R=$(CHOP_PICO_R)IOExp1_, PORT=$(I2C_TCA9555_PORT), IP_PORT=$(CHOP_PICO_PORT_NAME), ADDR=0, TIMEOUT=1")

AKI2CADT7420Configure($(I2C_ADT7420_PORT), $(CHOP_PICO_PORT_NAME), 1, "0x49", 1, 0, 0)
dbLoadRecords("AKI2C_ADT7420.db", "P=$(LEBT_P), R=$(CHOP_PICO_R)Temp1_, PORT=$(I2C_ADT7420_PORT), IP_PORT=$(CHOP_PICO_PORT_NAME), ADDR=0, TIMEOUT=1")

AKI2CLTC2991Configure($(I2C_LTC2991_PORT), $(CHOP_PICO_PORT_NAME), 1, "0x48", 0, 0, 1, 0, 0)
dbLoadRecords("AKI2C_LTC2991.db", "P=$(LEBT_P), R=$(CHOP_PICO_R)VMon1_, PORT=$(I2C_LTC2991_PORT), IP_PORT=$(CHOP_PICO_PORT_NAME), ADDR=0, TIMEOUT=1")

#- PicoScope 
epicsEnvSet(SCOPE_R, "PBI-Scope-001:")
epicsEnvSet(SCOPE_PORT, "PICO")
epicsEnvSet(PORT, "PICO")

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
epicsEnvSet("NUM_SAMPLES",                  "1000")
epicsEnvSet("MAX_SAMPLES",                  "100000")
epicsEnvSet("XSIZE",                        "$(MAX_SAMPLES)")
epicsEnvSet("YSIZE",                        "1")
epicsEnvSet("QSIZE",                        "20")
epicsEnvSet("NCHANS",                       "100")
epicsEnvSet("CBUFFS",                       "500")
epicsEnvSet("MAX_THREADS",                  "4")


PS4000AConfig("$(SCOPE_PORT)", "$(MAX_SAMPLES)", 4, 0, 0)

dbLoadRecords("ps4000a.template","P=$(LEBT_P)$(SCOPE_R), R=, PORT=$(SCOPE_PORT), ADDR=0, TIMEOUT=1, MAX_SAMPLES=$(MAX_SAMPLES)")

iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "PREFIX=$(LEBT_P)$(SCOPE_R), ADDR=0, ADDRCH=A0, NAME=A")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "PREFIX=$(LEBT_P)$(SCOPE_R), ADDR=1, ADDRCH=A1, NAME=B")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "PREFIX=$(LEBT_P)$(SCOPE_R), ADDR=2, ADDRCH=A2, NAME=C")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "PREFIX=$(LEBT_P)$(SCOPE_R), ADDR=3, ADDRCH=A3, NAME=D")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "PREFIX=$(LEBT_P)$(SCOPE_R), ADDR=4, ADDRCH=A4, NAME=E")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "PREFIX=$(LEBT_P)$(SCOPE_R), ADDR=5, ADDRCH=A5, NAME=F")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "PREFIX=$(LEBT_P)$(SCOPE_R), ADDR=6, ADDRCH=A6, NAME=G")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "PREFIX=$(LEBT_P)$(SCOPE_R), ADDR=7, ADDRCH=A7, NAME=H")

#- FUG @ HCK 200-12500 [CHOPPER]

epicsEnvSet(CHOP_R, "BMD-Chop-01:")
epicsEnvSet(CHOP_HOST, "bd-fug-hvps-01.tn.esss.lu.se")
epicsEnvSet(CHOP_CHANNAL, "$(LEBT_P)BMD-Chop-01")
epicsEnvSet(CHOP_INTERLOCK, "LEBT-LPS::BMD-Chop_HVEnCmd")

iocshLoad("$(fug_DIR)/fug_HV.iocsh", "Ch_name=$(CHOP_CHANNAL), IP_addr=$(CHOP_HOST), P=$(LEBT_P), R=$(CHOP_R), EGU=kv, ASLO=0.01")
dbLoadRecords("db/interlock.template", "P=$(LEBT_P), R=$(CHOP_R), Interlock_PV=$(CHOP_INTERLOCK)")
dbLoadRecords("db/set-up.template", "P=$(LEBT_P), R=$(CHOP_R)")
dbLoadRecords("db/chop_ps_limit.template", "P=$(LEBT_P), R=$(CHOP_R)")

dbLoadRecords("db/operational_parameters.db")
dbLoadRecords("db/autosave_parameters.db")

iocInit()

#-Can't run in db/operational_parameters due to value being set in module "xtpico"
dbpf LEBT-Chop:Ctrl-IM-01:VMon1_FactorV1 2.0
dbpf LEBT-Chop:Ctrl-IM-01:VMon1_FactorV2 2.0
dbpf LEBT-Chop:Ctrl-IM-01:VMon1_FactorV3 2.0
dbpf LEBT-Chop:Ctrl-IM-01:VMon1_FactorV4 2.0

